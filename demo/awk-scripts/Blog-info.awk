# .*blog/.*/.*\.html
# Adds an element with date created, date edited and estimated read time below h1

/<h1/ {
	$0 = $0 ORS "<div>Created: " created " | Last edited: " edited " | Read time: " (int(words / 180)+1) " min</div>"
}
