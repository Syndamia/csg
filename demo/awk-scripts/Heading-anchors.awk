# .*\.html
# Adds an HTML anchor after every header element (except h1), with an href to that header

/<h[23456]/ {
	match($0, /id="[^"]*"/)
	$0 = $0 ORS "<a href=\"#" substr($0, RSTART + 4, RLENGTH - 5) "\">&sect;</a>"
}
